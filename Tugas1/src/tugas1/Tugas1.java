package tugas1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class Tugas1 extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;


    private JButton jumlah;
    private JLabel bilangan1, bilangan2, hasil;
    private JTextField txt_bil1, txt_bil2, txt_hasil;

    public static void main(String[] args) {
        Tugas1 frame = new Tugas1();
        frame.setVisible(true);
        frame.setResizable(false);
    }

    public Tugas1() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        bilangan1 = new JLabel("Bilangan 1");
        bilangan1.setBounds(20, 20, 60, 20);
        contentPane.add(bilangan1);
        txt_bil1 = new JTextField();
        txt_bil1.setBounds(120, 20, 100, 20);
        contentPane.add(txt_bil1);

        bilangan2 = new JLabel("Bilangan2");
        bilangan2.setBounds(20, 50, 60, 20);
        contentPane.add(bilangan2);
        txt_bil2 = new JTextField();
        txt_bil2.setBounds(120, 50, 100, 20);
        contentPane.add(txt_bil2);

        hasil = new JLabel("Hasil");
        hasil.setBounds(20, 80, 60, 20);
        contentPane.add(hasil);
        txt_hasil = new JTextField();
        txt_hasil.setBounds(120, 80, 100, 20);
        txt_hasil.setEditable(false);
        contentPane.add(txt_hasil);

        jumlah = new JButton("Jumlah");
        jumlah.setBounds(120, 120, 80, 20);
        contentPane.add(jumlah);
        jumlah.addActionListener(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        int firstNumber = Integer.parseInt(txt_bil1.getText());
        int secondNumber = Integer.parseInt(txt_bil2.getText());
        int result = firstNumber + secondNumber;
        this.txt_hasil.setText(Integer.toString(result));

    }

}
